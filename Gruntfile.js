module.exports = function (grunt) {
  // Project configuration.
  grunt.initConfig({
    
      sass: {
      options: {
        sourcemap: 'none'
      },
      compile: {
        files: {
          'css/style.css': 'css/*.css',
          'scss/style.scss': 'scss/*.scss',
        }
      },
      watch: {

      }
    }
 });
 
  grunt.loadTasks('tasks');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');

  grunt.registerTask('default', ['sass', 'watch']);
  grunt.registerTask('default', ['test', 'build-contrib']);
};
