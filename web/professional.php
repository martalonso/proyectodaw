<?php 
   //session_start();

   //include '../app/db/dbconfig.php';
    include 'functions.php';
    $id = $_SESSION['id']; 
    $name = $_SESSION['name'];
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>WEB APPLICATION</title>
    <link rel="stylesheet" type="text/css" href="css/test.css">
</head>
<body>

<aside class="sidebar">
    <nav >
        <ul class="profileMenu">
            <li class="logoperfil">LOGO</li>
            <li class="userName"><a href="#sec-one"><img src="images/user.png"></a></li>
            <li class="projects"><a href="#sec-two"><img src="images/projects.png"></a></li>
            <li class="operations"><a href="#sec-three"><img src="images/purchase.png"></a></li>
        </ul>    
    </nav>
</aside>



<div class="container">
    
    <!-- LOG OUT -->

    <ul class="logProfile">
        <li><p>Bienvenido <?php echo $name ?></p></li>
        <li><a href="logout.php">Log out</a></li>
    </ul>


    <!-- DATOS GENERALES DE PROYECTOS -->
    <section class="section-profile" id="sec-one">


        <h3> MIS DATOS </h3>

        <form class="settings" id="settings" action="functions.php" method="POST">

           <?php settings($id); ?>
         
        </form> 

        <?php numProjects($id, 'professional'); ?>


    </section>

    <!-- DATOS DE PROYECTOS -->
    <section class="section-profile"  id="sec-two">

        <h3>MIS PROYECTOS </h3>
        <ul class='myprojects'>                        
            <li><strong>Proyecto</strong></li>
            <li><strong>Tipo:</strong></li>
            <li><strong>Descripción:</strong></li>
            <li><strong>Valor: </strong></li>
            <li><strong>Fecha: </strong></li>
            <li><strong>Client: </strong></li>
        </ul> 

        <?php profprojects($id); ?>

        <h3>LISTADO PROYECTOS </h3>
        
        <?php listprojects(); ?>

    </section>

    <section class="section-profile" id="sec-three">
        <h3> NUEVA PLANTILLA </h3>
       <form id="newtemplate" action="functions.php" method="POST" enctype="multipart/form-data">
            <label for="name">Nombre Plantilla: </label>
            <input type="text" name="t_name"/>
            <label for="type">Diseño/Desarrollo/marketing: </label>
            <input type="text" name="t_type"/>
            <label for="desc">Descripción de la plantilla:  </label>
            <input type="text" name="t_desc"/><br>
            <label for="price">Precio: </label>
            <input type="text" name="t_price"/>
            <label for="img">Imagen: </label>
            <input type="file" name="t_img" id="t_img"/>      
            <button type="submit" name="sendTemplate">Enviar Plantilla</button>
        </form>

        
        <h3>MIS PLANTILLAS </h3>

            <?php mytemplates($id); ?>

        
        <h3>DETALLE PLANTILLA </h3>  

            <?php templatedetail($id); ?>


    </section>


    <!-- DATOS PERSONALES -->
    <section class="section-profile" id="sec-four">


    </section>


</div>

</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script> 
<script src="scripts/script.js"></script>
</html>

