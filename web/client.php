<?php 
    //session_start();
    include 'functions.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>WEB APPLICATION</title>
    <link rel="stylesheet" type="text/css" href="css/test.css">
    <style type="css/text">

    p { color : #000;}
    </style>
</head>
<body>

<aside class="sidebar">
    <nav >
        <ul class="profileMenu">
            <li class="logoperfil">LOGO</li>
            <li class="userName"><a href="#sec-one"><img src="images/user.png"></a></li>
            <li class="projects"><a href="#sec-two"><img src="images/projects.png"></li>
            <li class="operations"><a href="#sec-three"><img src="images/purchase.png"></li>
            <!--<li class="settings"><img src="images/settings.png"></li>-->
        </ul>    
    </nav>
</aside>

<div class="container">
    
    <ul class="logProfile">
        <li><p>Bienvenido <?php echo $_SESSION['name'] ?></p></li>
        <li><a href="logout.php">Log out</a></li>
    </ul>

    <section class="section-profile" id="sec-one">

        <h3> MIS DATOS </h3>

        <form class="settings" id="settings" action="functions.php" method="POST">

           <?php settings($_SESSION['id']); ?>

        </form>

    </section>

    <section class="section-profile" id="sec-two">

        <h3> NUEVO PROYECTO </h3>

        <form id="newproject" action="functions.php" method="POST">
            <label for="name">Nombre Proyecto: </label>
            <input type="text" name="name"/>
            <label for="type">Diseño/Desarrollo/Marketing: </label>
            <input type="text" name="type"/>
            <label for="descrip">Descripción del Proyecto:  </label>
            <input type="text" name="descrip"/>
            <label for="value">Presupuesto: </label>
            <input type="text" name="value"/>                    
            <button type="submit" name="sendproject">Enviar Proyecto</button>
        </form>


        <h3>MIS PROYECTOS </h3>
        <table>
            
            <?php 
                $id = $_SESSION['id']; 
                myprojects($id);
            ?>

        </table>
        
     
    </section>

   
    <section class="section-profile" id="sec-three">
         
        <h3>MIS COMPRAS </h3>

        <p> Esta sección no está habilitada actualmente </p>
 
    </section>


</div>

</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script> 
<script src="scripts/script.js"></script>
</html>

