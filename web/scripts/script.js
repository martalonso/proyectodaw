$(document).ready(function(){
    init();
    portfolioMenu();
    actions();
    smoothscroll();
});

//  REGISTRO USUARIOS
function init(){

    $('#profregiter').hide();
    $('li[name="clientregister"]').click(function(){
        $(this).addClass('active');
        $('li[name="profRegister"]').removeClass('active');
        $('#profregiter').hide();
        $('#clientregiter').show();
    });


    $('li[name="profRegister"]').click(function(){
        $(this).addClass('active');
        $('li[name="clientregister"]').removeClass('active');
        $('#clientregiter').hide();
        $('#profregiter').show();
    });

}

//  GALERIA PLANTILLAS INDEX
function portfolioMenu(){

    $('.dsg').click(function(){
        $('.pdsg').show();
        $('.pdev').hide();
        $('.pmrk').hide();
    });
    $('.dev').click(function(){
        $('.pdsg').hide();
        $('.pdev').show();
        $('.pmrk').hide();
    });
    $('.mrk').click(function(){
        $('.pdsg').hide();
        $('.pdev').hide();
        $('.pmrk').show();

    });

}


//  SMOOTH SCROLL 
function smoothscroll(){

    $('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top -30
        }, 1000);
        return false;
      }
    }
  });

   $('a[href="#sec-one"').click(function(){
        $(this).css('opacity','1');
        $('a[href="#sec-two"').css('opacity','0.3');
        $('a[href="#sec-three"').css('opacity','0.3');
   });
    $('a[href="#sec-two"').click(function(){
        $(this).css('opacity','1');
        $('a[href="#sec-one"').css('opacity','0.3');
        $('a[href="#sec-three"').css('opacity','0.3');
   });
    $('a[href="#sec-three"').click(function(){
        $(this).css('opacity','1');
        $('a[href="#sec-two"').css('opacity','0.3');
        $('a[href="#sec-one"').css('opacity','0.3');
   });

}


//  ACCIONES AJAX
function actions(){

    // UNIRSE A UN PROYECTO
    btn = document.getElementsByClassName('addtoproject');    

    for (var i = 0; i < btn.length; i++) {

        btn[i].onclick = function(event){

            event.preventDefault();
            projectName = this.parentNode.parentNode.getElementsByTagName('td')[0].innerHTML;
           // console.log(projectName);
            enter =  confirm('Va a unirse a: "'+projectName+'"\n ¿Está seguro?');

              if (enter) {
                          
                            $.ajax({
                                type: 'POST',
                                url: 'functions.php',
                                dataType: 'json',
                                data: 'addtoproject='+projectName+'&action=addtoproject',
                                success: function(response){ 
                                    console.log(response.output);
                                    actions();
                                },
                                error: function(data, textStatus, jqXHR) {
                                    location.reload();
                                    console.log(textStatus);
                                    console.log(data);
                                }

                            });
                        return false;
                    } else {
                        return false;
                    }
                  
        }
    };



    // ACTUALIZAR TABLA LISTA PROYECTOS

    $('body').delegate('#listprojects','submit',function(){
        console.log('TEST');
         $.ajax({
            type: 'POST',
            url: 'functions.php',
            data: '&action=listprojects',
            dataType: 'json',
            success: function(response, string, jqXHR){
                console.log('okkkk');
                $( '#projectlist' ).html(response.output);
                actions();
            },
            error: function(data, textStatus, jqXHR) {
                console.log(textStatus);
                console.log(data);
            }
        });
     });


    //  EDITAR DATOS CUENTA
    $('#settings').submit(function(){
        event.preventDefault();

        data = $('#settings').serialize();

        console.log(data);

        $.ajax({
            type: 'POST',
            url: 'functions.php',
            data: data+'&action=editprofile',
            dataType: 'json',
            success: function(response, string, jqXHR){
                console.log(response.output);
                $( '#settings' ).html(response.output);
                actions();
            },
            error: function(data, textStatus, jqXHR) {
                console.log(textStatus);
                console.log(data);
            }
        });

    });


}
    

// AÑADIR NUEVO CAMPO VERIFICAR CONTRASEÑA 

$('.edit').on('click', function(){

    input = this.parentNode.getElementsByTagName('input')[0];

    input.css({border: '1px solid orange' });

    if(input.attr('type') == 'password'){


        $('.settings').append('<p>Confirma la nueva contraseña: <input type="password" name="pass2" style="border: 1px solid orange"/></p>');
    }

});