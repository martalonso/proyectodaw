<?php include '../app/db/dbconfig.php'; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>WEB APPLICATION</title>
    <link rel="stylesheet" type="text/css" href="css/test.css">
</head>
<body>

    
   <section >


          <h3> NUEVO PROYECTO </h3>
       <form class="project-form" id="newproject" action="" method="POST">
            <label for="name">Nombre Proyecto: </label>
            <input type="text" name="projectName"/>
            <label for="value">Descripción del Proyecto:  </label>
            <input type="text" name="description"/>
            <label for="value">Presupuesto: </label>
            <input type="text" name="projectValue"/>
            <label for="consult">Tipo: </label>
            <input type="text" name="projectType"/>            
            <button type="submit">Enviar Proyecto</button>
        </form>

        <hr>
        

         <h3>LISTADO PROYECTOS </h3>
        <table class="tablaprojets">
            
            <?php 

                

                $sql = mysqli_query($db, "SELECT * FROM project");


                while($row = mysqli_fetch_array($sql)){  

                   echo "            
                    <tr>                        
                        <td>". $row['name']."</td>
                        <td>". $row['descrip']."</td>
                        <td>". $row['value']."</td>
                        <td>". $row['date']."</td>
                        <td>". $row['name']."</td>
                        <td>". $row['idClient']."</td>
                        <td>". $row['idProf']."</td>

                    </tr> ";    

               }  

                ?>

        </table>
    </section>
    <section>
        <div class="templateDetail">
                 <h3>DETALLE PLANTILLA </h3>
           
            <?php 

                $sql = mysqli_query($db, "SELECT * FROM template WHERE id = 2");


                while($row = mysqli_fetch_array($sql)){  

                   echo " <div class=\"templateDetail left\">            
                    <p><img src=images/template/".$row['t_img']." style='width:30%;'>  
                    <ul>
                        <li>". $row['t_name']."</li>
                        <li>". $row['t_type']."</li>
                        <li>". $row['t_desc']."</li>                    
                        <li>". $row['t_price']."</li>
                        <li>". $row['t_date']."</li>
                        <li>". $row['t_prof']."</li>
                     </ul>
                    </p>
            
        </div>";    

               }  

                ?>
    </div>

        <h3> NUEVA PLANTILLA </h3>
       <form class="template-form" id="newtemplate" action="" method="POST">
            <label for="name">Nombre Plantilla: </label>
            <input type="text" name="t_name"/>
            <label >Tipo</label>
            <input type="text" name="t_type"/>
            <label for="desc">Descripción de la plantilla:  </label>
            <input type="text" name="t_desc"/>
            <label for="price">Precio: </label>
            <input type="text" name="t_price"/>
            <label >Img: </label>
            <input type="text" name="t_img"/> 
            <label for="datee">Fecha: </label>
            <input type="text" name="t_date"/>    
            <label for="prof">profesional: </label>
            <input type="text" name="t_prof"/>         
            <button type="submit">Enviar Plantilla</button>
        </form>

        <hr>


         <h3>LISTADO PLANTILLAS </h3>
        <table class="tablaprojets">
            
            <?php 

                $sql = mysqli_query($db, "SELECT * FROM template");


                while($row = mysqli_fetch_array($sql)){  

                   echo "            
                    <tr> 
                        <td> <img src=images/template/".$row['t_img']."></td>                    
                        <td>". $row['t_name']."</td>
                        <td>". $row['t_type']."</td>
                        <td>". $row['t_desc']."</td>                        
                        <td>". $row['t_price']."</td>
                        <td>". $row['t_date']."</td>
                        <td>". $row['t_prof']."</td>

                    </tr> ";    

               }  

                ?>

        </table>




    </section>


</body>
</html>

<?php

 if(isset($_POST['newproject'])){


           //INSERT INTO `webapp_db`.`project` (`idProy`, `name`, `descrip`, `date`, `type`, `idClient`, `idProf`) 
        //VALUES (NULL, 'proyecto 1', 'Lollipop ice cream', '2015-05-05', 'desarrollo', '4', '4'); 

            $projectName= $_POST['projectName'];
            $description = $_POST['description'];
            $value =  $_POST['projectValue'];
            $date = date("M-d-Y");
            $projectType= $_POST['projectType'];
            $value = $_POST['projectValue'];
            
            $imagenProject = $_POST['archiveProject'];

            $sql = mysqli_query($db, "INSERT INTO project 
                VALUES(0,'".$projectName."','".$description."','".$value."', '".$date."',".$projectType."',0,0)");

            if($sql){
                echo 'agregado';
                include 'operaciones.php';
            }else{
                echo "ha habido un error al agregar el proyecto ";
            }

    }


    if(isset($_POST['newtemplate'])){
        $t_name = $_POST['t_name'];
        $t_type = $_POST['t_type'];
        $t_desc = $_POST['t_desc'];
        $t_img = $_POST['t_img'];
        $t_price = $_POST['t_price'];
        $t_date = $_POST['t_date'];
        $t_prof = $_POST['t_prof'];

         $sql = mysqli_query($db, "INSERT INTO template 
            VALUES(0,'".$t_name."','".$t_type."','".$t_desc."', '".$t_img."','".$t_price."','".$t_date."','".$t_prof."')");

         //INSERT INTO `template`(`id`, `t_name`, `t_type`, `t_desc`, `t_img`, `t_price`, `t_date`, `t_prof`) 
         //VALUES ([value-1],[value-2],[value-3],[value-4],[value-5],[value-6],[value-7],[value-8])

         //INSERT INTO template VALUES (0,'template3','diseño','bla bla bla bla bla', 'tempate3.jpg', 50,'15/05/2015', 3)

        if($sql){
            echo 'agregado';
            include 'operaciones.php';
        }else{
            echo "ha habido un error al agregar la plantilla ";
        }


    }