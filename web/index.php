<?php include 'header.php'; ?>


    <section class="unete">
        
        <div class="option">
            <h2>PROFESIONAL</h2>
            <p>Lollipop bear claw chocolate powder cotton candy carrot cake. Apple pie cake bear claw toffee ice cream marzipan.</p>
            <button type="button">Únete a la comunidad</button>
        </div>
    </section>

    <section class="section-funcionamiento">
        <div class="col">
            <img src="images/graphic.png"/>
            <h3>DISEÑO</h3>
            <p>Lollipop bear claw chocolate powder cotton candy carrot cake. Apple pie cake bear claw toffee ice cream marzipan.</p>
        </div>
        <div class="col">  
            <img src="images/programming.png"/>
            <h3>DESARROLLO</h3>
            <p>Lollipop bear claw chocolate powder cotton candy carrot cake. Apple pie cake bear claw toffee ice cream marzipan.</p>
        </div>
        <div class="col">
            <img src="images/marketing.png"/>
            <h3>PUBLICACIÓN</h3>
            <p>Lollipop bear claw chocolate powder cotton candy carrot cake. Apple pie cake bear claw toffee ice cream marzipan.</p>
        </div>
    </section>

    
    <!--<section class="section-project">
        <h2>CREA UN PROYECTO</h2>
         <form class="project-form" id="newproject" action="functions.php" method="POST">
            <label for="name">Nombre Proyecto: </label>
            <input type="text" name="projectName"/>
            <label for="consult">Tipo: </label>
            <input type="text" name="projectType"/>
            <label for="value">Presupuesto: </label>
            <input type="text" name="ProjectValue"/>
            <label for="desc">Descripción del Proyecto: </label>
            <input type="text" name="desc"/>
            <button type="submit" name="submitProject">Enviar Proyecto</button>
        </form>
    </section>-->
    


    <section class="section-portfolio">
        <button class="dsg" type="button">Diseño</button>
        <button class="dev" type="button">Desarrollo</button>
        <button class="mrk" type="button">Marketing 2.0</button>
        <div class="row">
            <ul class="items">
                <?php templateImages(); ?>
            </ul>           
        </div>
    </section>

<footer class="indexfooter"></footer>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script> 
<script src="scripts/script.js"></script>
</html>