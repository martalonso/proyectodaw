<?php

session_start();

    if(!isset($_SESSION["session_username"])) {
        header("Location:login.php");
    } else {
    
?>
     
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>client profile</title>
        <link rel="stylesheet" type="text/css" href="css/styles.css">
    </head>
    <body>
        <header>
                <h1>WEB APP</h1>
                <nav>
                    <ul class="log">
                        <li><a href="logout.php">Log out</a></li>
                    </ul>
                </nav>
            </header>
     <div id="container">
        <aside>
            
            <h3><?php echo $name; ?></h3>

            <ul class="profileMenu">
                <li>MIS PROYECTOS</li>
                <li>MIS COMPRAS</li>
                <li>CONFIGURACIÓN</li>
            </ul>
        </aside>
        <section>
            



        </section>
    </div>

    
    <?php include("includes/footer.php"); ?>
     
    <?php
    }
?>


   